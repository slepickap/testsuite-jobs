def reportFileLocation = 'report.html'

def reportResult(reportFile) {
  publishHTML([
    allowMissing: false,
    alwaysLinkToLastBuild: false,
    keepAll: false,
    reportDir: '',
    reportFiles: reportFile,
    reportName: 'SSH Report'
  ])
}

node {
  stage("Clone") {
    git "https://gitlab.com/slepickap/testsuite.git"
  }

  stage("Test") {
    withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: "gitlab", usernameVariable: 'GITLAB_USER', passwordVariable: 'GITLAB_PW']]) {
      withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: "ssh", usernameVariable: 'SSH_HOST', passwordVariable: 'SSH_PW']]) {
        try {
          sh "pytest tests/ssh_test.py --html=${reportFileLocation}"
        } catch(err) {
          reportResult(reportFileLocation)
          error "${err}"
        }
      }
    }
  }

  stage("Report") {
    reportResult(reportFileLocation)
  }
}